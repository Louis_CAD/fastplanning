<?php
require 'codb.php';
$db->exec("CREATE TABLE IF NOT EXISTS activities (idAct VARCHAR PRIMARY KEY)");
$db->exec("CREATE TABLE IF NOT EXISTS users (idUser VARCHAR PRIMARY KEY, password VARCHAR)");
$db->exec("CREATE TABLE IF NOT EXISTS planning (idAct VARCHAR, idUser VARCHAR, time TIMESTAMP, FOREIGN KEY(idAct) references activities(idAct), FOREIGN KEY(idUser) references users(idUser))");
$activities = array("Java", "Python", "Anglais", "Repos", "Café", "PHP");
foreach ($activities as $activity) {
	insertactivity($activity, $db);
}
$sql = $db->prepare("SELECT * from activities");
$sql->execute();
while ($res = $sql->fetch()) {
	echo $res['idAct'].'<br/>';
}
?>