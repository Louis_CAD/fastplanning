<?php
session_start();
if(isset($_POST['id']) && isset($_POST['password'])) {
    $id = $_POST['id'];
    $pw = $_POST['password'];
    if(!empty($id) && !empty($pw)) {
    	require 'codb.php';
    	if (login($id, $pw, $db)) {
    		$_SESSION['id'] = $id;
    		header('Location: index.php');
    	} else {
    		header('Location: connexion.php?erreur=wrongids');
    	}
    } else {
    	header('Location: connexion.php?erreur=empty');
    }
} else {
	header('Location: connexion.php?erreur=unset');
}
?>