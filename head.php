<?php
if ($title != "connexion") {
	if (!isset($_SESSION['id'])) {
		header('Location: connexion.php');
	}
}
?>
<!DOCTYPE html>
<html>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link rel="stylesheet" type="text/css" href="style.css"/>