<?php
//Connexion à la base de données
date_default_timezone_set('Europe/Paris');
setlocale (LC_TIME, 'fr_FR.utf8','fra');
try {
	$db = new PDO('sqlite:planning.sqlite3');
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	// Fonctions
	function insertActivity($idAct, $db) {
		$sql = $db->prepare("INSERT INTO activities (idAct) VALUES(?)");
		$sql->execute(array($idAct));
	}
	function newActivity($nomAct, $id, $when, $db) {
		$sql = $db->prepare("SELECT COUNT(*) as nb FROM planning WHERE time=?");
		$sql->execute(array($when));
		$res = $sql->fetch();
		if ($res['nb']>0)
			return false;
		else {
			$sql = $db->prepare("INSERT INTO planning (idAct, idUser, time) VALUES(?,?,?) ");
			$sql->execute(array($nomAct, $id, $when));
			return true;
		}
	}
	function insertUser($id, $password, $db) {
		$sql = $db->prepare("SELECT * FROM users");
		$sql->execute();
		if ($sql->rowCount()>=10)
			return 'maxReached';
		$sql = $db->prepare("SELECT COUNT(*) as nb FROM users WHERE idUser=?");
		$sql->execute(array($id));
		$res = $sql->fetch();
		if ($res['nb']>0)
			return 'existingID';
		$sql = $db->prepare("INSERT INTO users (idUser, password) VALUES(?,?)");
		$sql->execute(array($id, $password));
		return 'success';
	}
	function login($id, $password, $db) {
		$sql = $db->prepare('SELECT COUNT(*) as nb FROM users WHERE idUser = :id AND password = :pw');
		$sql->execute(array('id' => $id, 'pw' => $password));
		$res = $sql->fetch();
		if ($res['nb']>0) {
			return true;
		} else {
			return false;
		}
	}
}
catch (PDOException $e) {
	echo $e->getMessage();
}
?>