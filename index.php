<?php
session_start();
require 'codb.php';
$title = "Accueil";
if (isset($_POST['h']) && isset($_POST['j']) && isset($_POST['m']) && isset($_POST['a'])) {
	$time = mktime($_POST['h'], 0, 0, $_POST['m'], $_POST['j'], $_POST['a']);
	if (newActivity($_POST['activity'], $_SESSION['id'], $time, $db)) {
		header("Location: index.php?j=".$_POST['j'].'&m='.$_POST['m'].'&a='.$_POST['a']);
	} else {
		header("Location: index.php");
	}
}
if (isset($_GET['j']) && isset($_GET['m']) && isset($_GET['a'])) {
	$cj = $_GET['j'];
	$cm = $_GET['m'];
	$ca = $_GET['a'];
} else {
	$cj = date('d');
	$cm = date('m');
	$ca = date('Y');
	header('Location: index.php?j='.$cj.'&m='.$cm.'&a='.$ca);
}
require 'head.php';
require 'header.php';
?>
<section>
	<form id="display" method="get" action="index.php">
	<h3>Jour à afficher</h3>
		<select name="j">
			<?php
			for ($j=1; $j<=31; $j++) {
				if ($j == $cj) {
					echo '<option selected value="'.$j.'">'.$j.'</option>';
				} else {
					echo '<option value="'.$j.'">'.$j.'</option>';
				}
			}
			?>
		</select>
		<select name="m">
			<?php
			for ($m=1; $m<=12; $m++) {
				if ($m == $cm) {
					echo '<option selected value="'.$m.'">'.$m.'</option>';
				} else {
					echo '<option value="'.$m.'">'.$m.'</option>';
				}
			}
			?>
		</select>
		<select name="a">
			<?php
			for ($a=2014; $a<=2033; $a++) {
				if ($a == $ca) {
					echo '<option selected value="'.$a.'">'.$a.'</option>';
				} else {
					echo '<option value="'.$a.'">'.$a.'</option>';
				}
			}
			?>
		</select>
		<input type="submit" value="Valider"/>
	</form>


	<?php
	if (isset($_GET['j']) && isset($_GET['m']) && isset($_GET['a'])) {
		$j = $_GET['j'];
		$m = $_GET['m'];
		$a = $_GET['a'];
	} else {
		$j = date('d');
		$m = date('m');
		$a = date('Y');
	}
	$url = 'index.php?j='.$j.'&m='.$m.'&a='.$a;
	?>
	<form id="add" method="post" action=<?php echo '"'.$url.'"'; ?>>
		<h3>Ajouter une activité</h3>
		<select name="activity">
			<?php
			$sql = $db->prepare("SELECT idAct FROM activities");
			$sql->execute();
			while($res = $sql->fetch()) {
				echo '<option value="'.$res['idAct'].'">'.$res['idAct'].'</option>';
			}
			?>
		</select>
		<select name="h">
			<?php
			for ($h=8; $h<=20; $h++) {
				echo '<option value="'.$h.'">'.$h.'</option>';
			}
			?>
		</select>
		<select name="j">
			<?php
			for ($j=1; $j<=31; $j++) {
				if ($j == $cj) {
					echo '<option selected value="'.$j.'">'.$j.'</option>';
				} else {
					echo '<option value="'.$j.'">'.$j.'</option>';
				}
			}
			?>
		</select>
		<select name="m">
			<?php
			for ($m=1; $m<=12; $m++) {
				if ($m == $cm) {
					echo '<option selected value="'.$m.'">'.$m.'</option>';
				} else {
					echo '<option value="'.$m.'">'.$m.'</option>';
				}
			}
			?>
		</select>
		<select name="a">
			<?php
			for ($a=2014; $a<=2033; $a++) {
				if ($a == $ca) {
					echo '<option selected value="'.$a.'">'.$a.'</option>';
				} else {
					echo '<option value="'.$a.'">'.$a.'</option>';
				}
			}
			?>
		</select>
		<input type="submit" value="Valider"/>
	</form>
	<br/>
	<br/>
	<article>
	<h2>Planning du <?php echo $cj.'/'.$cm.'/'.$ca; ?></h2>
	<table>
		<thead>
			<td>Heure</td>
			<td>Activité</td>
		</thead>
		<tbody>
		<?php
		for ($h=8; $h < 21; $h++) {
			?>
			<tr>
				<td><?php echo $h.'h'; ?></td>
				<?php
				echo '<td';
				$time = mktime($h, 0, 0, $cm, $cj, $ca);
				$sql = $db->prepare("SELECT idAct FROM planning WHERE time=? AND idUser=?");
				$sql->execute(array($time, $_SESSION['id']));
				if ($res = $sql->fetch()) {
					echo ' style="border-color: #33b5e5;">';
					echo $res['idAct'];
				} else {
					echo '>';
				}
				echo '</td>';
				?>
			</tr>
				<?php
		}
		?>
		</tbody>
	</table>
	</article>
	</section>
