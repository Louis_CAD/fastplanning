<?php
session_start();
if(isset($_POST['id']) && isset($_POST['password'])) {
    $id = $_POST['id'];
    $pw = $_POST['password'];
    if(!empty($id) && !empty($pw)) {
    	require 'codb.php';
    	$insertStatus = insertUser($id, $pw, $db);
    	if ($insertStatus == "success") {
    		$_SESSION['id'] = $id;
    		header('Location: index.php');
    	}
    	else {
    		header('Location: signup.php?erreur='.$insertStatus);
        }
    }
} else {
    header('Location: signup.php?erreur=empty');
}
?>